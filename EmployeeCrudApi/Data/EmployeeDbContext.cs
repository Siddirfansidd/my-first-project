﻿using EmployeeCrudApi.Model;
using Microsoft.EntityFrameworkCore;
using System;

namespace EmployeeCrudApi.Data
{
    public class EmployeeDbContext : DbContext
    {
        public EmployeeDbContext(DbContextOptions<EmployeeDbContext> options)
            : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string connectionString = Environment.GetEnvironmentVariable("DATABASE_CONNECTION");
                optionsBuilder.UseNpgsql(connectionString);
                //optionsBuilder.UseNpgsql(connectionString,builder => {
                //builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null);
                //});
                optionsBuilder.EnableSensitiveDataLogging(true);
            }
        }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
    }
}
