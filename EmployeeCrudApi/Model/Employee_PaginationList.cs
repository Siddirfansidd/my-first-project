﻿using System.Collections.Generic;

namespace EmployeeCrudApi.Model
{
    public class Employee_PaginationList
    {
        public Pagination Pagination { get; set; }
        public List<Employee> list { get; set; }
    }
}
