﻿namespace EmployeeCrudApi.Model
{
    public class DepartmentRequestDto
    {
        public string Name { get; set; }
    }
}
