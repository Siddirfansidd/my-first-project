﻿namespace EmployeeCrudApi.Model
{
    public class Pagination
    {
        public int CurrentPage { get; set; }
        public int Count { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
    }
}
