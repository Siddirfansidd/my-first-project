﻿using EmployeeCrudApi.Data;
using EmployeeCrudApi.Model;
using EmployeeCrudApi.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeCrudApi.Repository
{
    public class EmployeeRepo : IEmployeeRepo
    {
        private readonly EmployeeDbContext _employeeDbContext;
        private readonly IErrorHelper _errorHelper;

        public EmployeeRepo(EmployeeDbContext employeeDbContext, IErrorHelper errorHelper)
        {
            _employeeDbContext = employeeDbContext;
            _errorHelper = errorHelper;
        }
        public List<Employee> GetAllEmployees()
        {
            var employees = _employeeDbContext.Employees.Include(x => x.Department).ToList();
            return employees;
        }
        public Employee_PaginationList GetEmployee_PaginationList(int currentPage = 1, int PageSize = 10)
        {
            var model = new Employee_PaginationList();
            var EmployeeList = _employeeDbContext.Employees.Include(x => x.Department).Skip((currentPage -1)* PageSize).Take(PageSize).ToList();

            model.list = EmployeeList;
            int totalRecord = _employeeDbContext.Employees.Count();
            var page = new Pagination
            {
                Count = totalRecord,
                CurrentPage = currentPage,
                PageSize = PageSize,
                TotalPages = (int)Math.Ceiling(decimal.Divide(totalRecord, PageSize)),
            };
            model.Pagination = page;
            return model;
        }
        public Employee CreateEmployee(EmployeeRequestDto employeeRequestDto)
        {
            if (employeeRequestDto.FirstName == null)
                _errorHelper.ThrowError("firstname is required");

            if (employeeRequestDto.LastName  == null)
            if (employeeRequestDto.LastName  == null)
                _errorHelper.ThrowError("Lastname is required");

            if (employeeRequestDto.Address == null)

            if (employeeRequestDto.Email  == null)
            if (employeeRequestDto.Email  == null)
                _errorHelper.ThrowError("Email is required");

            if (employeeRequestDto.MobileNo == null)
                _errorHelper.ThrowError("MobileNo is required");

            var data = new Employee();
            data.FirstName = employeeRequestDto.FirstName;
            data.LastName = employeeRequestDto.LastName;
            data.Address = employeeRequestDto.Address;
            data.Email = employeeRequestDto.Email;
            data.MobileNo = employeeRequestDto.MobileNo;
            data.DepartmentId = employeeRequestDto.DepartmentId;
            _employeeDbContext.Add(data);
            _employeeDbContext.SaveChanges();
            return GetEmployeeById(data.Id);
        }

        public List<Employee> CreateMultipleEmployee(List<EmployeeRequestDto> employeeRequests)
        {
            var employees = new List<Employee>();
            foreach (var employeeRequest in employeeRequests)
            {
                var employee = new Employee();
                employee.FirstName = employeeRequest.FirstName;
                employee.LastName = employeeRequest.LastName;
                employee.Address = employeeRequest.Address;
                employee.Email = employeeRequest.Email;
                employee.MobileNo = employeeRequest.MobileNo;
                employee.DepartmentId = employeeRequest.DepartmentId;
                employees.Add(employee);
            }

            _employeeDbContext.AddRange(employees);
            _employeeDbContext.SaveChanges();
            var employeeIds = employees.Select(y => y.Id).ToArray();

            return GetMultipleEmployeeIds(employeeIds);
        }

        private List<Employee> GetMultipleEmployeeIds(int[] employeeIds)
        {
            return _employeeDbContext.Employees.Include(x => x.Department).Where(x => employeeIds.Contains(x.Id)).ToList();
        }

        public Employee GetEmployeeById(int id)
        {
            return _employeeDbContext.Employees.Include(x => x.Department).FirstOrDefault(x => x.Id == id);
        }
        public async Task<bool> DeleteEmployeeById(int id)
        {
            var obj = await _employeeDbContext.Employees.FindAsync(id);
            _employeeDbContext.Employees.Remove(obj);
            return await _employeeDbContext.SaveChangesAsync() > 0;
        }
        public async Task<Employee> UpdateEmployeeAsync(int id, EmployeeRequestDto employeeRequestDto)
        {
            var employeeRecord = await _employeeDbContext.Employees.FindAsync(id);
            if (employeeRecord == null)
                _errorHelper.ThrowError("record not found");

            employeeRecord.FirstName = employeeRequestDto.FirstName;
            employeeRecord.LastName = employeeRequestDto.LastName;
            employeeRecord.Address = employeeRequestDto.Address;
            employeeRecord.Email = employeeRequestDto.Email;
            employeeRecord.MobileNo = employeeRequestDto.MobileNo;
            employeeRecord.DepartmentId = employeeRequestDto.DepartmentId;
            _employeeDbContext.Update(employeeRecord);
            await _employeeDbContext.SaveChangesAsync();
            return GetEmployeeById(employeeRecord.Id);
        }
    }
}
