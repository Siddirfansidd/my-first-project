﻿using EmployeeCrudApi.Data;
using EmployeeCrudApi.Model;
using EmployeeCrudApi.Services;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeCrudApi.Repository
{
    public class DepartmentRepo : IDepartmentRepo
    {
        private readonly EmployeeDbContext _employeeDbContext;
        private readonly IErrorHelper _errorHelper;

        public DepartmentRepo(EmployeeDbContext employeeDbContext, IErrorHelper errorHelper)
        {
            _employeeDbContext=employeeDbContext;
            _errorHelper=errorHelper;
        }
        public List<Department> GetAllDepartments()
        {
            var departments = _employeeDbContext.Departments.ToList();
            return departments;
        }
        public Department CreateDepartment(DepartmentRequestDto departmentRequestDto)
        {
            var result = new Department();
            result.Name = departmentRequestDto.Name;
            _employeeDbContext.Add(result);
            _employeeDbContext.SaveChanges();
            return GetDepartmentById(result.Id);
        }
        public Department GetDepartmentById(int id)
        {
            return _employeeDbContext.Departments.FirstOrDefault(x => x.Id == id);
        }
        public async Task<bool> DeleteDepartmentById(int id)
        {
            var obj = await _employeeDbContext.Departments.FindAsync(id);
            _employeeDbContext.Departments.Remove(obj);
            return await _employeeDbContext.SaveChangesAsync()>0;
        }
        public async Task<Department> UpdateDepartment(int id, DepartmentRequestDto departmentRequestDto)
        {
            var record = await _employeeDbContext.Departments.FindAsync(id);
            if (record == null)
                _errorHelper.ThrowError("record not found");

            record.Name = departmentRequestDto.Name;
            _employeeDbContext.Departments.Update(record);
            await _employeeDbContext.SaveChangesAsync();
            return record;
        }
    }
}
