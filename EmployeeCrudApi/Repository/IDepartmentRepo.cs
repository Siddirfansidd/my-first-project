﻿using EmployeeCrudApi.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeCrudApi.Repository
{
    public interface IDepartmentRepo
    {
        List<Department> GetAllDepartments();
        Department CreateDepartment(DepartmentRequestDto departmentRequestDto);
        Department GetDepartmentById(int id);
        Task<bool> DeleteDepartmentById(int id);
        Task<Department> UpdateDepartment(int id, DepartmentRequestDto departmentRequestDto);

    }
}
