﻿using EmployeeCrudApi.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EmployeeCrudApi.Repository
{
    public interface IEmployeeRepo
    {
        List<Employee> GetAllEmployees();
        public Employee_PaginationList GetEmployee_PaginationList(int currentPage = 1, int PageSize = 10);
        Employee CreateEmployee(EmployeeRequestDto employeeRequestDto);
        Employee GetEmployeeById(int id);
        Task<bool> DeleteEmployeeById(int id);
        Task<Employee> UpdateEmployeeAsync(int id, EmployeeRequestDto employeeRequestDto);
        public List<Employee> CreateMultipleEmployee(List<EmployeeRequestDto> employeeRequests);
    }
}
