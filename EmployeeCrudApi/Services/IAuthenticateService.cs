﻿using EmployeeCrudApi.Model;

namespace EmployeeCrudApi.Services
{
    public interface IAuthenticateService
    {
        User Authenticate(string userName, string password);
    }
}
