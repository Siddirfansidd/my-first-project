﻿using System;

namespace EmployeeCrudApi.Services
{
    public class ErrorHelper :IErrorHelper
    {
        public void ThrowError(string message)
        {
            throw new Exception(message);
        }
    }
}
