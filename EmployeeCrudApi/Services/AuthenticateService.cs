﻿using EmployeeCrudApi.Model;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System;
using System.Linq;

namespace EmployeeCrudApi.Services
{
    public class AuthenticateService : IAuthenticateService 
    {
        private readonly AppSettings _appSettings;
        public AuthenticateService(IOptions<AppSettings> appSettings)
        {
            _appSettings=appSettings.Value;
        }
        private List<User> users = new List<User>()
        {
            new User{UserId = 1,
            FirstName = "Irfan",
            LastName = "Ahmad",
            UserName = "Asus",
            Password = "Zebronics"}
        };
        public User Authenticate(string userName, string password)
        {
            var user = users.SingleOrDefault(x => x.UserName == userName && x.Password == password);

            //return null if user is not found
            if (user == null)
                return null;

            //If user is found 
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Key);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new System.Security.Claims.ClaimsIdentity(new Claim[]
             {
                 new Claim(ClaimTypes.Name, user.UserId.ToString()),
                 new Claim(ClaimTypes.Role, "Admin"),
                 new Claim(ClaimTypes.Version, "V3.1"),
                 new Claim(ClaimTypes.Email, "sidirfanahmad48578@gmail.com")
             }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);
            user.Password = null;
            return user;
        }
    }
}
