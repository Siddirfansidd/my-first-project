﻿namespace EmployeeCrudApi.Services
{
    public interface IErrorHelper
    {
        public void ThrowError(string message);

    }
}
