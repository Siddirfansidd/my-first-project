﻿using EmployeeCrudApi.Model;
using EmployeeCrudApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeCrudApi.Controllers
{
    [Route("api/token")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticateService _authenticateService;

        public AuthenticationController(IAuthenticateService authenticateService)
        {
            _authenticateService=authenticateService;
        }
        [HttpPost]
        public IActionResult Post([FromBody] User model)
        {
         var user = _authenticateService.Authenticate(model.UserName, model.Password);
            if (user == null)
                return BadRequest(new { message = "UserName or Password is incorrect" });

            return Ok(user);
        }
    }
}
