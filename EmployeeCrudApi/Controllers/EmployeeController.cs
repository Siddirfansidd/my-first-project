﻿using EmployeeCrudApi.Model;
using EmployeeCrudApi.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace EmployeeCrudApi.Controllers
{
    [Authorize]
    [Route("api")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepo _employeeRepo;
        private readonly IDepartmentRepo _departmentRepo;

        public EmployeeController(IEmployeeRepo employeeRepo, IDepartmentRepo departmentRepo)
        {
            _employeeRepo = employeeRepo;
            _departmentRepo = departmentRepo;
        }

        #region Employee

        [HttpGet("Employee")]
        public IActionResult GetAllEmployees()
        {
            var obj = _employeeRepo.GetAllEmployees();
            return Ok(obj);
        }

        [HttpGet("Employee/{id}")]
        public IActionResult GetEmployeeById(int id)
        {
            return Ok(_employeeRepo.GetEmployeeById(id));
        }

        [HttpGet("EmployeePaginationList")]

        public ActionResult<Employee_PaginationList> GetEmployee_PaginationList(int currentPage = 1, int PageSize = 10)
        {
            var obj = _employeeRepo.GetEmployee_PaginationList(currentPage, PageSize);
            return Ok(obj);
        }

        [HttpPost("Employee")]
        public ActionResult<Employee> CreateEmployee(EmployeeRequestDto employeeRequestDto)
        {
            try
            {
                return Ok(_employeeRepo.CreateEmployee(employeeRequestDto));
            }
            catch (Exception ex)
            {
                var result = new ErrorResponeDto();
                result.message = ex.Message;
                return BadRequest(result);
            }
        }

        [HttpPut("Employee/{id}")]
        public async Task<IActionResult> UpdateEmployeeById(int id, EmployeeRequestDto employeeRequestDto)
        {
            try
            {
                return Ok(await _employeeRepo.UpdateEmployeeAsync(id, employeeRequestDto));
            }
            catch (Exception ex)
            {
                var result = new ErrorResponeDto();
                result.message = ex.Message;
                return BadRequest(result);
            }
        }

        [HttpDelete("Employee/{id}")]
        public async Task<IActionResult> DeleteEmployeeById(int id)
        {
            var obj = await _employeeRepo.DeleteEmployeeById(id);
            return Ok(obj);
        }

        [HttpPost("MultipleEmployee")]
        public IActionResult CreateMultipleEmployee(List<EmployeeRequestDto> employeeRequests)
        {
            var result = _employeeRepo.CreateMultipleEmployee(employeeRequests);
            return Ok(result);
        }

        #endregion

        #region Department

        [HttpGet("Department")]
        public IActionResult GetAllDepartments()
        {
            return Ok(_departmentRepo.GetAllDepartments());
        }

        [HttpGet("Department/{id}")]
        public IActionResult GetDepartmentById(int id)
        {
            return Ok(_departmentRepo.GetDepartmentById(id));
        }

        [HttpPost("Department")]
        public ActionResult<Department> CreateDepartment(DepartmentRequestDto departmentRequestDto)
        {
            return Ok(_departmentRepo.CreateDepartment(departmentRequestDto));
        }

        [HttpPut("Department/{id}")]
        public async Task<IActionResult> UpdateDepartmentById(int id, DepartmentRequestDto departmentRequestDto)
        {
            try
            {
                return Ok(await _departmentRepo.UpdateDepartment(id, departmentRequestDto));
            }
            catch (Exception ex)
            {
                var result = new ErrorResponeDto();
                result.message = ex.Message;
                return BadRequest(result);
            }
        }

        [HttpDelete("Department/{id}")]
        public async Task<IActionResult> DeleteDepartmentById(int id)
        {
            var obj = await _departmentRepo.DeleteDepartmentById(id);
            return Ok(obj);
        }
        #endregion
    }
}

